



import UIKit


class ViewController: UIViewController,UITableViewDelegate,
                      UITableViewDataSource{
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return countriesName.count
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
    tableView.rowHeight=100
    
    let label=cell.contentView.viewWithTag(1)as! UILabel
    
    label.text = countriesName[indexPath.row]
     
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let detailVC = self.storyboard?.instantiateViewController(identifier: "detailVC") as! DetailViewController
    
    detailVC.flag=countriesFlag[indexPath.row]
    
    
    self.navigationController?.pushViewController(detailVC, animated: true)
  }
 
  
  
  @IBOutlet weak var tableView: UITableView!
  
 
  var countriesName = [String]()
  var countriesFlag=[String]()
  
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

      tableView.delegate=self
      tableView.dataSource=self
      
      
        for code in NSLocale.isoCountryCodes  {

            let flag = String.emojiFlag(for: code)
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])

            if let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) {
                
              countriesName.append(name)
              countriesFlag.append(flag!)
              
            }else{
                 //"Country not found for code: \(code)"
            }
        }

      print(countriesName)
        print(countriesFlag)
    }
}


extension String {

    static func emojiFlag(for countryCode: String) -> String! {
        func isLowercaseASCIIScalar(_ scalar: Unicode.Scalar) -> Bool {
            return scalar.value >= 0x61 && scalar.value <= 0x7A
        }

        func regionalIndicatorSymbol(for scalar: Unicode.Scalar) -> Unicode.Scalar {
            precondition(isLowercaseASCIIScalar(scalar))

            // 0x1F1E6 marks the start of the Regional Indicator Symbol range and corresponds to 'A'
            // 0x61 marks the start of the lowercase ASCII alphabet: 'a'
            return Unicode.Scalar(scalar.value + (0x1F1E6 - 0x61))!
        }

        let lowercasedCode = countryCode.lowercased()
        guard lowercasedCode.count == 2 else { return nil }
        guard lowercasedCode.unicodeScalars.reduce(true, { accum, scalar in accum && isLowercaseASCIIScalar(scalar) }) else { return nil }

        let indicatorSymbols = lowercasedCode.unicodeScalars.map({ regionalIndicatorSymbol(for: $0) })
        return String(indicatorSymbols.map({ Character($0) }))
    }
  
  
  
  
  
  
}
