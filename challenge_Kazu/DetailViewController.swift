//
//  DetailViewController.swift
//  Kazu_challenge
//
//  Created by 山口千寿 on 2021/11/09.
//

import UIKit

class DetailViewController: UIViewController {

  
  var flag=String()
  
  @IBOutlet weak var flagLabel: UILabel!
  
  
    override func viewDidLoad() {
        super.viewDidLoad()

      flagLabel.text=flag
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
